package repository;

import com.google.common.collect.Lists;
import entity.ProductEntity;
import entity.SellerEntity;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Slf4j
public class SellerRepository extends Repository {

    public void save(SellerEntity sellerEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO seller (login, name) VALUES (?,?)")) {

            preparedStatement.setString(1, sellerEntity.getLogin());
            preparedStatement.setString(2, sellerEntity.getName());

            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save seller error!", e);
        }
    }

    public List<SellerEntity> get() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT s.id, s.name, s.login FROM seller s")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<SellerEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<SellerEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(SellerEntity.builder()
                    .id(resultSet.getInt("id"))
                    .name(resultSet.getString("name"))
                    .login( resultSet.getString("login"))
                    .build());
        }
        return result;
    }

}
