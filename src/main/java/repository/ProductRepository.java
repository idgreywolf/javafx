package repository;

import com.google.common.collect.Lists;
import entity.ProductEntity;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ProductRepository extends Repository {

    public void saveProduct(ProductEntity productEntity) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement("INSERT INTO product (product_name, qnty) VALUES (?,?)")) {

            preparedStatement.setString(1, productEntity.getProductName());
            preparedStatement.setInt(2, productEntity.getQnty());

            preparedStatement.execute();
        } catch (SQLException e) {
            log.error("Save product error!", e);
        }
    }

    public List<ProductEntity> getProducts() {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT p.id, p.product_name, p.qnty FROM product p")) {
            return processResultSet(preparedStatement.executeQuery());
        } catch (SQLException e) {
            log.error("Get products error!", e);
        }
        return Collections.emptyList();
    }

    private List<ProductEntity> processResultSet(ResultSet resultSet) throws SQLException {
        List<ProductEntity> result = Lists.newArrayList();
        while (resultSet.next()) {
            result.add(ProductEntity.builder()
                    .id(resultSet.getInt("id"))
                    .productName(resultSet.getString("product_name"))
                    .qnty( resultSet.getInt("qnty"))
                    .build());
        }
        return result;
    }

}
