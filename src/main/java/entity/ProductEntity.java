package entity;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductEntity {

    private int id;
    private String productName;
    private int qnty;
}
