package entity;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SellerEntity {

    private int id;
    private String name;
    private String login;
}
