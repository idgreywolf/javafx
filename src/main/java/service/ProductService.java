package service;

import entity.ProductEntity;
import javafx.collections.FXCollections;
import javafx.scene.control.TableView;
import repository.ProductRepository;

import java.util.List;

public class ProductService {

    private final ProductRepository productRepository = new ProductRepository();

    public void addProduct(String productName, int productQnty) {
        productRepository.saveProduct(
                ProductEntity.builder()
                        .productName(productName)
                        .qnty(productQnty).build()
        );
    }

    public List<ProductEntity> getProducts() {
        return productRepository.getProducts();
    }


}
