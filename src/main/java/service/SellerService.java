package service;

import entity.ProductEntity;
import entity.SellerEntity;
import repository.ProductRepository;
import repository.SellerRepository;

import java.util.List;

public class SellerService {

    private final SellerRepository sellerRepository = new SellerRepository();

    public void add(String name, String login) {
        sellerRepository.save(
                SellerEntity.builder()
                        .name(name)
                        .login(login)
                        .build()
        );
    }

    public List<SellerEntity> get() {
        return sellerRepository.get();
    }


}
