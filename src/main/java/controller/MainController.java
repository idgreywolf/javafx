package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

import java.io.IOException;


public class MainController {

    @FXML
    Pane content;

    @FXML
    private void clickProduct(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("products.fxml"));
        content.getChildren().setAll(root);

    }

    @FXML
    private void clickSellerMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sellers.fxml"));
        content.getChildren().setAll(root);

    }

}
