package controller;

import entity.ProductEntity;
import entity.SellerEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import service.ProductService;
import service.SellerService;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsController {

    private final ProductService productService = new ProductService();

    @FXML
    private TextField productName;

    @FXML
    private TextField productQnty;

    @FXML
    private TableView<ProductEntity> productTable;

    @FXML
    private TableColumn<ProductEntity, Integer> productIdColumn;

    @FXML
    private TableColumn<ProductEntity, String> productNameColumn;

    @FXML
    private TableColumn<ProductEntity, Integer> productQntyColumn;


    @FXML
    private void addProduct(ActionEvent event) {
        String name = productName.getText();
        Integer qnty = Integer.parseInt(productQnty.getText());
        productService.addProduct(name, qnty);
        refreshProductTable();
    }


    public void initialize() {
        productIdColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, Integer>("id"));

        productNameColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, String>("productName"));

        productQntyColumn.setCellValueFactory(
                new PropertyValueFactory<ProductEntity, Integer>("qnty"));

        refreshProductTable();

    }


    private void refreshProductTable() {
        List<ProductEntity> products = productService.getProducts();
        productTable.setItems(FXCollections.observableArrayList(products));
    }



}
