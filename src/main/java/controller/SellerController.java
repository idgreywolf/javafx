package controller;

import entity.ProductEntity;
import entity.SellerEntity;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import service.SellerService;

import java.util.List;

public class SellerController {

    private final SellerService sellerService = new SellerService();

    @FXML
    private TextField sellerLogin;

    @FXML
    private TextField sellerName;

    @FXML
    private TableView<SellerEntity> sellerTable;

    @FXML
    private TableColumn<SellerEntity, Integer> sellerIdColumn;

    @FXML
    private TableColumn<SellerEntity, String> sellerLoginColumn;

    @FXML
    private TableColumn<SellerEntity, String> sellerNameColumn;

    @FXML
    private void addSeller(ActionEvent event) {
        String name = sellerName.getText();
        String login = sellerLogin.getText();
        sellerService.add(name, login);
        refreshSellerTable();
    }

    public void initialize() {

        sellerIdColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, Integer>("id"));

        sellerLoginColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("login"));

        sellerNameColumn.setCellValueFactory(
                new PropertyValueFactory<SellerEntity, String>("name"));

        refreshSellerTable();

    }

    private void refreshSellerTable() {
        List<SellerEntity> sellers = sellerService.get();
        sellerTable.setItems(FXCollections.observableArrayList(sellers));
    }

}
